import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
import AttendConference from './AttendConferenceForm'
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm'
import PresentationForm from './PresentationForm'
import MainPage from './MainPage'
import { BrowserRouter, Routes, Route } from "react-router-dom";

export default function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="presentation" element={<PresentationForm />} />
          <Route path="attendees" element={<AttendeesList attendees={props.attendees}/>} />
          <Route path="attendees/new" element={<AttendConference />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="conferences/new" element={<ConferenceForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}
