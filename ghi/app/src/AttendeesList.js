import React, {useEffect, useState } from 'react';

export default function AttendeesList(props) {
  const [attendees, setAttendees] = useState([])
  const getAttendees = async function() {
    const url = 'http://localhost:8001/api/attendees/'
    const response = await fetch(url)

    if (response.ok) {
      const { attendees } = await response.json()
      setAttendees(attendees)
    }
  }

  useEffect(() => {
    getAttendees()
  }, [])

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Conference</th>
        </tr>
      </thead>
      <tbody>
        {attendees?.map(attendee => {
          return (
            <tr key={attendee.href}>
              <td>{ attendee.name }</td>
              <td>{ attendee.conference }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
