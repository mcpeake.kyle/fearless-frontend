import React, { useEffect, useState } from 'react'

export default function PresentationForm() {
    const [formData, setFormData] = useState({
        presenter_name: 'presenter_name',
        presenter_email: 'presenter_email',
        company_name: 'company_name',
        title: 'title',
        synopsis: 'synopsis',
        conference: 'conference',
    })

    const [conferences, setConferences] = useState([])

    const handleInputChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setConferences(data.conferences)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.presenter_name = formData.presenter_name
        data.presenter_email = formData.presenter_email
        data.company_name = formData.company_name
        data.title = formData.title
        data.synopsis = formData.synopsis
        data.conference_id = parseInt(formData.conference.match(/[0-9]/g).join(""));
        const url = `http://localhost:8000${formData.conference}presentations/`

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFormData({
                presenter_name: '',
                presenter_email: '',
                company_name: '',
                title: '',
                synopsis: '',
                conference: '',
            })
        }
    }

    return (
        <main>
            <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                    <div className="form-floating mb-3">
                        <input placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"
                        value={formData.presenter_name} onChange={handleInputChange} />
                        <label htmlFor="presenter_name">Presenter name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"
                        value={formData.presenter_email} onChange={handleInputChange} />
                        <label htmlFor="presenter_email">Presenter email</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"
                        value={formData.company_name} onChange={handleInputChange} />
                        <label htmlFor="company_name">Company name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="Title" required type="text" name="title" id="title" className="form-control"
                        value={formData.title} onChange={handleInputChange} />
                        <label htmlFor="title">Title</label>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="synopsis">Synopsis</label>
                        <textarea className="form-control" id="synopsis" rows="3" name="synopsis" value={formData.synopsis} onChange={handleInputChange} ></textarea>
                    </div>
                    <div className="mb-3">
                        <select required name="conference" id="conference" className="form-select" value={formData.conference} onChange={handleInputChange}>
                        <option value="">Choose a conference</option>
                        {conferences.map(conference => {
                            return (
                                <option key={conference.href} value={conference.href}>{conference.name}</option>
                            )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
            </div>
        </main>
            );
        }
