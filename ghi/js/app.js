function createCard(name, description, pictureUrl, dates, subtitle) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${subtitle}</h6>
          <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
          <p>${dates}</p>
        </div>
      </div>
    `;
  }

function formatDate(starts, ends) {
  const starting = starts.slice(0, 10)
  const ending = ends.slice(0, 10)
  let start_year = starting.slice(0,4)
  let end_year = ending.slice(0,4);
  let start_month = starting.slice(5,7);
  if (start_month[0] === "0") {
    start_month = start_month.slice(1, 2)
  }
  let end_month = ending.slice(5,7)
  if (end_month[0] === "0") {
    end_month = end_month.slice(1, 2)
  }
  let start_day = starting.slice(8,11)
  let end_day = ending.slice(8,11)
  return `${start_month}/${start_day}/${start_year} - ${end_month}/${end_day}/${end_year}`
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);
      if (!response.ok) {
        // Figure out what to do when the response is bad
      const warningSection = document.querySelector('.warning')
      warningSection.innerHTML += "An error occured attempting to load conferences"
        // console.log('Response is not ok')
        // alert("Response is not ok")
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;

            const starts = (details.conference.starts);
            const ends = (details.conference.ends);
            const dates = formatDate(starts, ends);
            const subtitle = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, dates, subtitle);
            const column = document.querySelector('.col');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.log('Error:', e)
      alert("Error:", e)

    }

  });





// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//         console.log('Response is not ok')
//       } else {
//         const data = await response.json();

//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;

//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//           const details = await detailResponse.json();

//         const descriptionTag = document.querySelector('.card-text');
//         descriptionTag.innerHTML = details.conference.description;

//         const imageTag = document.querySelector('.card-img-top')
//         imageTag.src = details.conference.location.picture_url;
//         }
//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//       console.log('Error:', e)
//     }

//   });
